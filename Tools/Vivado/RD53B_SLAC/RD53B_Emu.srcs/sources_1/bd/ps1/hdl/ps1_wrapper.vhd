--Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
--Date        : Mon Nov 11 17:19:18 2019
--Host        : RATBAG running 64-bit major release  (build 9200)
--Command     : generate_target ps1_wrapper.bd
--Design      : ps1_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ps1_wrapper is
  port (
    clk : in STD_LOGIC;
    dip_switches_4bits_tri_i : in STD_LOGIC_VECTOR ( 3 downto 0 );
    gpio1_tri_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
    gpio1_tri_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    gpio2_tri_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
    gpio2_tri_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    pit1_toggle : out STD_LOGIC;
    push_buttons_5bits_tri_i : in STD_LOGIC_VECTOR ( 4 downto 0 );
    reset_n : in STD_LOGIC;
    uart_rxd : in STD_LOGIC;
    uart_txd : out STD_LOGIC
  );
end ps1_wrapper;

architecture STRUCTURE of ps1_wrapper is
  component ps1 is
  port (
    uart_rxd : in STD_LOGIC;
    uart_txd : out STD_LOGIC;
    gpio1_tri_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
    gpio1_tri_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    gpio2_tri_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
    gpio2_tri_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    reset_n : in STD_LOGIC;
    clk : in STD_LOGIC;
    pit1_toggle : out STD_LOGIC;
    dip_switches_4bits_tri_i : in STD_LOGIC_VECTOR ( 3 downto 0 );
    push_buttons_5bits_tri_i : in STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  end component ps1;
begin
ps1_i: component ps1
     port map (
      clk => clk,
      dip_switches_4bits_tri_i(3 downto 0) => dip_switches_4bits_tri_i(3 downto 0),
      gpio1_tri_i(31 downto 0) => gpio1_tri_i(31 downto 0),
      gpio1_tri_o(31 downto 0) => gpio1_tri_o(31 downto 0),
      gpio2_tri_i(31 downto 0) => gpio2_tri_i(31 downto 0),
      gpio2_tri_o(31 downto 0) => gpio2_tri_o(31 downto 0),
      pit1_toggle => pit1_toggle,
      push_buttons_5bits_tri_i(4 downto 0) => push_buttons_5bits_tri_i(4 downto 0),
      reset_n => reset_n,
      uart_rxd => uart_rxd,
      uart_txd => uart_txd
    );
end STRUCTURE;
