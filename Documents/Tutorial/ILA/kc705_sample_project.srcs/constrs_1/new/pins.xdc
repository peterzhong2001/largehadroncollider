# Clock Constraints - For timing only 
create_clock -period 5.0 -waveform {0 2.5} [get_ports sysclk_p]

# Pin Placement

# Reset Pin
set_property PACKAGE_PIN G12 [get_ports rst]
set_property IOSTANDARD LVCMOS25 [get_ports rst]

# SysClk
set_property PACKAGE_PIN AD11 [get_ports sysclk_n]
set_property IOSTANDARD LVDS [get_ports sysclk_n]
set_property PACKAGE_PIN AD12 [get_ports sysclk_p]
set_property IOSTANDARD LVDS [get_ports sysclk_p]

# LED Ports
set_property -dict { PACKAGE_PIN F16  IOSTANDARD LVCMOS25  } [get_ports { led[7] }];
set_property -dict { PACKAGE_PIN E18  IOSTANDARD LVCMOS25  } [get_ports { led[6] }];
set_property -dict { PACKAGE_PIN G19  IOSTANDARD LVCMOS25  } [get_ports { led[5] }];
set_property -dict { PACKAGE_PIN AE26 IOSTANDARD LVCMOS25  } [get_ports { led[4] }];
set_property -dict { PACKAGE_PIN AB9  IOSTANDARD LVCMOS18  } [get_ports { led[3] }];
set_property -dict { PACKAGE_PIN AC9  IOSTANDARD LVCMOS18  } [get_ports { led[2] }];
set_property -dict { PACKAGE_PIN AA8  IOSTANDARD LVCMOS18  } [get_ports { led[1] }];
set_property -dict { PACKAGE_PIN AB8  IOSTANDARD LVCMOS18  } [get_ports { led[0] }];

# SW Ports
set_property PACKAGE_PIN AA12 [get_ports sw[0]]
set_property IOSTANDARD LVCMOS18 [get_ports sw[0]]
set_property PACKAGE_PIN AG5 [get_ports sw[1]]
set_property IOSTANDARD LVCMOS18 [get_ports sw[1]]
set_property PACKAGE_PIN AB12 [get_ports sw[2]]
set_property IOSTANDARD LVCMOS18 [get_ports sw[2]]
set_property PACKAGE_PIN AC6 [get_ports sw[3]]
set_property IOSTANDARD LVCMOS18 [get_ports sw[3]]