`timescale 1ns / 1ps

module top(
    input logic  sysclk_p, sysclk_n,
    input logic  rst,
    input logic  [3:0] sw,
    
    output logic [7:0] led
);

    logic [31:0] curr_counter, next_counter;
    logic [1:0] curr_state, next_state;
    logic sysclk;
    
    parameter [1:0] hold = 2'b00, forward = 2'b01, backward = 2'b10, by_four = 2'b11;
    
    IBUFGDS #(.DIFF_TERM("TRUE"), .IOSTANDARD("LVDS_25")) ii (
        .I(sysclk_p),
        .IB(sysclk_n),
        .O(sysclk)
    );
    
    ila_0 ila_debug(
    .clk(sysclk),
    .probe0(curr_state),
    .probe1(next_state),
    .probe2(curr_counter),
    .probe3(next_counter),
    .probe4(sw)
    );
    
    
    always_ff@(posedge sysclk or posedge rst) begin
        if(rst) begin
            curr_state <= hold;
            curr_counter <= 32'h0;
        end else begin
            curr_state[1:0] <= next_state[1:0];
            curr_counter[31:0] <= next_counter[31:0];
        end
    end
    
    always_comb begin
        if(sw[0]) begin
            next_state = hold;
        end else if(sw[1]) begin
            next_state = forward;
        end else if(sw[2]) begin
            next_state = backward;
        end else if(sw[3]) begin
            next_state = by_four;
        end else begin
            next_state = curr_state;
        end
    end
    
    always_comb begin
        case(curr_state) 
            hold: begin
                next_counter = curr_counter;
            end
            forward: begin
                next_counter = curr_counter + 'b1;
            end
            backward: begin
                next_counter = curr_counter - 'b1;
            end
            by_four: begin
                next_counter = curr_counter + 'b100;
            end        
            default: begin
                next_counter = 32'hX;  
            end
        endcase
    end
    
    assign led[7:0] = curr_counter[29:22];
endmodule
